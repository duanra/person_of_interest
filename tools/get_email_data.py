#!usr/bin/env python3

import os
import sys
import urllib.request
import tarfile
import shutil

from IPython import embed

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


cd = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.realpath(os.path.join(cd, os.path.pardir, 'data'))
mail_dir = os.path.join(data_dir, 'maildir')
mail_tar = os.path.join(data_dir, 'enron_mail_20150507.tar.gz')

# embed()
if not(os.path.exists(data_dir)):
	os.makedirs(data_dir)

print('[Info] the following structure needs to be changed manually')
print('\tmv maildir/stokley-c/chris_stokley/* maildir/stokley-c/')
print('\trm -r maildir/stokley-c/chris_stokley/')


if os.path.exists(mail_dir) and os.listdir(mail_dir):
	print('mail directory already exists: %s'%mail_dir)

	if os.path.exists(mail_tar):
		print('enron dataset already downloaded: %s'%mail_tar)
		sys.exit(0)


print('downloading enron email dataset...')
url = 'https://www.cs.cmu.edu/~./enron/enron_mail_20150507.tar.gz'

with urllib.request.urlopen(url) as filereq, open(mail_tar, 'wb') as fileout:
	# fileout.write(filereq.read())
	shutil.copyfileobj(filereq, fileout) # avoid to load everything into memory
	print('saved at: %s'%mail_tar)

while True:
	x = input('extract the dataset now? (y/n): ')
	
	if x == 'y':
		print('unpacking...')
		with tarfile.open(mail_tar, 'r:gz') as f:
			f.extractall(path=data_dir)
		break

	elif x=='n':
		break

print('::done::')
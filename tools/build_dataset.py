#!usr/bin/env python3

import os
import sys
import pickle
import numpy as np
import pandas as pd

from data_parser import EmailParser
from IPython import embed

pd.set_option('display.max_rows', 20)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


cd = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.realpath(os.path.join(cd, os.path.pardir, 'data'))


def get_finances():

	file = os.path.join(data_dir, 'enron61702insiderpay.txt')
	colnames = [
		'insider', 'salary', 'bonus',
		'longterm_incentive', 'deferred_income',
		'deferral_payments', 'loan_advances',
		'other', 'expenses', 'director_fees',
		'total_payments', 'exercised_stock_options',
		'restricted_stocks', 'restricted_stock_deffered',
		'total_stock_value'
	]

	def format_insider(s):
		s = s.replace('--', ' ') #', '
		s = s.replace('&', ' ')
		s = s.title()
		return s

	finances = pd.read_csv(
		file, delim_whitespace=True, names=colnames,
		skiprows=7, nrows=152-7, na_values='NaN', thousands=',',
		converters=dict(insider=format_insider)
	)

	return finances[colnames]


def get_users():

	file = os.path.join(data_dir, 'user_details.txt')
	users = pd.read_csv(
		file, delimiter=r'\s{2,}', header=0, engine='python'
	)

	return users


def get_emails():

	file = os.path.join(data_dir, 'emails_parsed.pkl')	
	with open(file, 'rb') as fhandler:
		emails = pickle.load(fhandler)

	return emails


# finances = get_finances()
users = get_users()
emails = get_emails()


for _, u in users.iterrows():
	
	e = emails[u.basename]
	from_ = e.from_dic
	to_ = e.to_dic
	cc_ = e.cc_dic

	from_this = from_.pop(u.user_email, 0) # sent
	to_this = to_.pop(u.user_email, 0) # received
	to_this += cc_.pop(u.user_email, 0)

	from_others = from_ # received
	to_others = to_ # sent
	while len(cc_):
		user_email, count = cc_.popitem()
		to_others[user_email] = count + to_others.get(user_email, 0)

	break

embed()
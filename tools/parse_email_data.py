#!usr/bin/env python3

import os
import sys
import subprocess as sub
import numpy as np
import pandas as pd
import pickle
import utils

from tabulate import tabulate
from IPython import embed
from data_parser import EmailParser

pd.set_option('display.max_rows', 20)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


cd = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.realpath(os.path.join(cd, os.path.pardir, 'data'))
mail_dir = os.path.join(data_dir, 'maildir')


def parse_emails(n=None):
	"""
	n: (int) number of users to parse
	"""

	emails = {}
	user_dirs = utils.find_directories(mail_dir)

	for user_dir in user_dirs[:n]:
	# user_dir = '/mnt/DATA/Repos/git/person_of_interest/data/maildir/skilling-j'
		print(user_dir)

		user_files = utils.find_files(user_dir, recursive=True)
		basename = os.path.basename(user_dir)
		parser = EmailParser(user_dir, user_files)
		parser.read_addresses()

		emails[basename] = parser

	return emails


if __name__ == '__main__':

	emails = parse_emails()
	
	while True:
		"""
		pickled object need to have access to the class and module
		when unpickling, i.e. with explicit import
		"""
		save = input('save parsed emails? (y/n): ')

		if save == 'y':
			f = os.path.join(data_dir, 'emails_parsed.pkl')
			with open(f, 'wb') as fhandler:
				print('writing %s'%f)
				pickle.dump(emails, fhandler)
			break

		elif save == 'n':
			break

	print('::done::')

	embed()
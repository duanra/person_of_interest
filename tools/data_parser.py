#!usr/bin/env python3

import os
import platform
import re
import numpy as np
from functools import wraps

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# https://stackoverflow.com/questions/4075753/how-to-delete-a-folder-that-name-ended-with-a-dot
if platform.system() == 'Windows':
	MOD = "\\\\?\\"
else:
	MOD = ""

class UserParser:

	def __init__(self, user_dir, user_folders, user_files):

		self.user_dir = user_dir
		self.user_folders = user_folders
		self.user_files = user_files

		self.user_basename = os.path.basename(user_dir)
		self.nfiles = len(user_files)
		self.nfolders = len(user_folders)
		
		self.read_user_detail()


	def _check_user_detail(self, user_email, user_name):

		basename = self.user_basename.split('-')[0]

		print('User directory: %s'%self.user_dir) 
		
		if (not basename in user_email.lower()) or '<' in user_email:
			print('  Possible wrong parsing of user_email: %s'%user_email)
			new_user_email = input('  New user_name (enter to validate): ')
			if new_user_email:
				user_email = new_user_email
		
		if not basename in user_name.lower() or '<' in user_name:
			print('  Possible wrong parsing of user_name: %s'%user_name)
			new_user_name = input('  New user_name (enter to validate): ')
			if new_user_name:
				user_name = new_user_name

		return user_email, user_name


	def read_user_detail(self):
		
		for box in ['sent_items', 'sent', '_sent_mail', 'inbox']:

			if box in  self.user_folders:
				email = os.path.join(self.user_dir, box, '1.')

				if box=='inbox':
					pattern1, pattern2 = 'To: ', 'X-To: '
				else:
					pattern1, pattern2 = 'From: ', 'X-From: '

				break

		with open(MOD + email, 'r') as fhandler:

			for line in fhandler: # do not readlines()

				if line.startswith(pattern1):
					user_email = line.split(': ')[1].split(', ')[0].strip()

				elif line.startswith(pattern2):
					user_name = line.split(': ')[1].split(' <')[0].strip()
					user_name = user_name.replace(',', '')
					user_name = user_name.replace('.', '')
					break
	
		self.user_email, self.user_name =\
			self._check_user_detail(user_email, user_name)

		return self.user_email, self.user_name



class EmailParser:
	""""
	TODO: keep track of path/to/email in from's , to's, and cc's.
	"""

	def __init__(self, user_dir, user_files):
		
		self.user_dir = user_dir
		self.user_files = user_files
		self.user_basename = os.path.basename(user_dir)

		self.email_ids = []
		self.from_addrs, self.to_addrs, self.cc_addrs = [], [], []
	

	def _read_metadata(self, email):

		with open(MOD + email, 'rt', errors='replace') as fhandler:
			
			# print(email)
			fhandler.readline()
			email_id = fhandler.readline().split(': ')[1].strip()
			if email_id in self.email_ids:
				return None

			self.email_ids.append(email_id)
			pattern = r'[\w\.-]+@[\w\.-]+\.\w+'

			from_str = fhandler.readline()
			# some from_str has no valid address --> None --> error
			# from_addr = re.search(pattern, from_str).group(0)
			from_addr = re.findall(pattern, from_str)
			self.from_addrs.extend(from_addr)

			to_str, cc_str = '', ''
			while True:
				line = fhandler.readline()

				if line.startswith('To: '):
					to_str += line.strip()
				
				elif not(cc_str) and line.startswith('\t'):
					to_str += line.strip()

				elif line.startswith('Cc: '):
					cc_str += line.strip()

				elif to_str and line.startswith('\t'):
					cc_str += line.strip()

				elif line.startswith('Mime-'):
					break

			self.to_addrs += re.findall(pattern, to_str)
			self.cc_addrs += re.findall(pattern, cc_str)

	
	def read_addresses(self):

		for email in self.user_files:
			self._read_metadata(email)

		self.from_dic = dict(zip(*
			np.unique(self.from_addrs, return_counts=True)
		))

		self.to_dic = dict(zip(*
			np.unique(self.to_addrs, return_counts=True)
		))

		self.cc_dic = dict(zip(*
			np.unique(self.cc_addrs, return_counts=True)
		))
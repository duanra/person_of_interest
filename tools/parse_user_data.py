#!usr/bin/env python3

import os
import sys
import subprocess as sub
import numpy as np
import pandas as pd
import utils

from tabulate import tabulate
from IPython import embed
from data_parser import UserParser

pd.set_option('display.max_rows', 20)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


cd = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.realpath(os.path.join(cd, os.path.pardir, 'data'))
mail_dir = os.path.join(data_dir, 'maildir')


def parse_user_details(n=None):
	"""
	n: (int) number of users to parse
	wrong parsing: 
		lysa.akin@enron.com  --> mary.hain@enron.com
		Lysa Akin --> Hain Mary

		legal <.taylor@enron.com> --> .taylor@enron.com
		
		no.address@enron.com --> jane.tholt@enron.com
		@ECT --> Jane M Tholt
	"""
	users = []
	user_dirs = utils.find_directories(mail_dir, recursive=False)

	for user_dir in user_dirs[:n]:

		user_folders = utils.find_directories(
			user_dir, recursive=False, basename=user_dir)
		user_files = utils.find_files(
			user_dir, recursive=True, basename=user_dir)

		parser = UserParser(user_dir, user_folders, user_files)
		users.append(
			(parser.user_basename, parser.user_email, parser.user_name))

	return users


if __name__ == '__main__':

	users = parse_user_details()
	table = tabulate(
		users, headers=['basename', 'user_email', 'user_name'],
		tablefmt='plain')

	while True:
		save = input('save user details? (y/n): ')

		if save == 'y':
			f = os.path.join(data_dir, 'user_details.txt')
			print('writing %s'%f)
			print(table, file=open(f, 'w'))
			break

		elif save=='n':
			break
	
	print('::done::')

	embed()
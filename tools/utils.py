#!usr/bin/env python3

import os
import warnings
from natsort import natsorted

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def find_directories(path, recursive=False, basename=False):
	"""
	find all directories in path.
	basename: bool or str (indicating the prefix to drop)
	"""

	if not os.path.exists(path):
		warnings.warn('[Warning] find_files(): the directory does not exist')
		return

	ret = []
	for root, directories, files in os.walk(path):
		for directory in directories:

			if directory.startswith('.') or directory.endswith('~'):
				continue

			else:
				if basename is True:
					ret.append(directory)
	
				else:
					directory = os.path.join(root, directory)
					if isinstance(basename, str):
						# prefix = os.path.commonprefix([basename, directory])
						directory = directory.split(basename)[1][1:]
					
					ret.append(directory)

		if not recursive:
			break

	return natsorted(ret)


def find_files(path, recursive=False, basename=False):
	"""
	find all files in path.
	basename: bool or str (indicating the prefix to drop)
	"""

	if not os.path.exists(path):
		warnings.warn('[Warning] find_files(): the directory does not exist')
		return

	ret = []
	for root, directories, files in os.walk(path):
		for file in files:

			if file.startswith('.') or file.endswith('~'):
				continue
			
			else:
				if basename is True:
					ret.append(file)

				else:
					file = os.path.join(root, file)
					if isinstance(basename, str):
						file = file.split(basename)[1][1:]
	
					ret.append(file)

		if not recursive:
			break

	return natsorted(ret)